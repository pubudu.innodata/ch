import os
import win32com.client
import sys

wdFormatPDF = 17

inputFilePath = sys.argv[1]
outputFilePath = sys.argv[2]

inputFile = os.path.basename(inputFilePath)

outputFile = os.path.basename(outputFilePath)

file = open(outputFile, "w")
file.close()
word = win32com.client.Dispatch('Word.Application')
doc = word.Documents.Open(inputFilePath)
doc.SaveAs(outputFilePath, FileFormat=wdFormatPDF)
doc.Close()
word.Quit()



