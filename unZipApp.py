import zipfile
import sys
import os.path


def main():
    zipFilePath = sys.argv[1]
    extractFilePathWithFileName = sys.argv[2]

    basename = os.path.basename(extractFilePathWithFileName)

    dirname = os.path.dirname(extractFilePathWithFileName)

    extensions = ('.docx')
    zip_file = zipfile.ZipFile(zipFilePath, 'r')
    extractFile = [zip_file.extract(file, dirname)
                   for file in zip_file.namelist() if file.endswith(extensions)]

    os.rename(extractFile[0], dirname + '/' + basename)

    zip_file.close()


if __name__ == '__main__':
    main()
